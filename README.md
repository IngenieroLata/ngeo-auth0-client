This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

# Auth0 React Client

## Table of Contents

1. [Folder Structure](#folder-structure)
1. [Working](#working)
   1. [Installing](#installing)
   1. [Useful Scripts](#useful-scripts)
   1. [Styles and Conventions](#styles-and-conventions)

### Folder Structure

As a reference for redux-saga folder structure we implement [react-router](https://github.com/erikras/ducks-modular-redux) and [Feature First (Pods)](http://engineering.kapost.com/2016/01/organizing-large-react-applications/) approach for the component structure.

```
└── src
    └── view
        └── RouterView
            ├── ComponentName1.jsx
            ├── ComponentName1.scss
            ├── ComponentName1.test.js
            ├── Inner Component
            |   └── ...
            └── ...
    ├── services
        ├── axios-instance.jsx
        ├── service.jsx
        └── service.test.js
    └── index.js
    └── App.jsx
```

Where:

- `src`: Contains the technologies, folders, and the source code of Profile Center.
- `├── components`: React components in charge of presentation tending to be stateless, reusable and receive data and callbacks exclusively via props.
- `├── services`: Contains services wrapper and all required API calls required across the application.
- `└── index`: App entry point.
- `└── App`: Main component. Contains routes using [react-router](https://github.com/ReactTraining/react-router) and web template for all pages.

**[▲ Back to Top](#table-of-contents)**

## Working

### Prerequisites

To setup the `profile-center` project, install the following tools on your computer:

- [Node.js](https://nodejs.org/) `^8.9.4`.  
  To install Node.js, download it from the [Node.js webpage](https://nodejs.org/).

- [npm](https://www.npmjs.com/). `^5.6.0`  
  Usually comes with node js.

### Installing Profile Center Locally

To setup the project locally:

1. Login to the Nat Geo npm registry:

   ```bash
   $ npm login
   ```

   Note: Npm will ask for credentials. Please ask your technical leader on how to get them.

2. Install the dependencies:

   ```bash
   $ npm install
   ```

3. Run local dev server:
   ```bash
   $ npm start
   ```
4. It will automatically open your default browser on http://localhost:3000

### Useful Scripts

The table below contains the scripts available for the project:

| Command             | Description                                           |
| ------------------- | ----------------------------------------------------- |
| `npm build`         | Builds the Profile Center into /build prod optimized. |
| `npm start`         | Starts in development mode with livereload .          |
| `npm test`          | Runs all the unit test suite and watcher.             |
| `npm test-coverage` | Runs all the unit test suite and show coverage data.  |

**[▲ Back to Top](#table-of-contents)**

### Styles and Conventions

Styles are enforced on this repository by `style-by-convention-loader` this package allows you to create a `foo.js` file and it will automatically looks for a `foo.scss` file to import.

Profile Center uses the following style guides:

**[Javascript Style Guide](https://github.com/natgeo/javascript)**

**[React Style Guide](https://github.com/airbnb/javascript/tree/master/react)**

**CSS Style Guide**

Exception: Use cammelCase for class names.
