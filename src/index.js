import { render } from 'react-dom'
import React from 'react'
import App from './App'
import './index.scss'

render(<App />, document.getElementById('ngeo-auth0-client'))
